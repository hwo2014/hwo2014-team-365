using System;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class Piece {

        public double Length;
        public bool? Switch;
        public int? Radius;
        public double? Angle;

        //Empty Constructor
        public Piece(){}

    }
}