using System;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class Dimensions {

        public double Length;
        public double Width;
        public double GuideFlagPosition;

        //Empty Constructor
        public Dimensions(){}

    }
}