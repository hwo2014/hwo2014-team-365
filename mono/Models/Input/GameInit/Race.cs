namespace HelloOpenWorld.Models.Input.GameInit
{
    public class Race {

        public Track Track;
        public Car[] Cars;
        public RaceSession RaceSession;
    }
}