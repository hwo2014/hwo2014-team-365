using System;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class GameInit {

        public Race Race;
    }
}