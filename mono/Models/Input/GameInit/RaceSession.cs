using System;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class RaceSession {

        public int Laps;
        public int MaxLapTimeMs;
        public bool QuickRace;

        //Empty Constructor
        public RaceSession(){}

    }
}