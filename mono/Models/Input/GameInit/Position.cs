using System;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class Position {

        public double X;
        public double Y;

        //Empty Constructor
        public Position(){}

    }
}