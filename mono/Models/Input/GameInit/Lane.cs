using System;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class Lane {

        public int DistanceFromCenter;
        public int Index;

        //Empty Constructor
        public Lane(){}

    }
}