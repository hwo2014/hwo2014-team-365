using System;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class StartingPoint {

        public Position Position;
        public double Angle;

        //Empty Constructor
        public StartingPoint(){}

    }
}