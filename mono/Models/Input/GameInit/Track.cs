using System;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class Track {

        public string Id;
        public string Name;
        public Piece[] Pieces;
        public Lane[] Lanes;
        public StartingPoint StartingPoint;

        //Empty Constructor
        public Track(){}

    }
}