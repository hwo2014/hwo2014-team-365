using System;

using HelloOpenWorld.Models.Input.Shared;

namespace HelloOpenWorld.Models.Input.GameInit
{
    [Serializable]
    public class Car {

        public Id Id;
        public Dimensions Dimensions;

        //Empty Constructor
        public Car(){}

    }
}