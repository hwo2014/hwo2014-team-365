using HelloOpenWorld.Models.Input.Shared;

namespace HelloOpenWorld.Models.Input.CarPositions
{
    public class CarPosition
    {

        public Id Id;
        public double Angle;
        public PiecePosition PiecePosition;

    }
}