﻿namespace HelloOpenWorld.Models.Input.CarPositions
{
    public class Lane
    {

        public int StartLaneIndex;
        public int EndLaneIndex;
    }
}