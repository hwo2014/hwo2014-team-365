namespace HelloOpenWorld.Models.Input.CarPositions
{
    public class PiecePosition
    {

        public int PieceIndex;
        public double InPieceDistance;
        public Lane Lane;
        public int Lap;

        //Empty Constructor
        public PiecePosition() { }

    }
}