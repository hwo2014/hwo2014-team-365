using Newtonsoft.Json;

namespace HelloOpenWorld.Models.Output
{
    class Throttle: SendMsg<object>  {

        [JsonProperty(PropertyName = "value")]
        public double value;

        public Throttle(double value) {
            this.value = value;
        }



        protected override object MsgData()
        {
            return this.value;
        }

        protected override string MsgType() {
            return "throttle";
        }
    }
}