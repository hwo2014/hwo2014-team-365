using Newtonsoft.Json;

namespace HelloOpenWorld.Models.Output
{
    public class Join : SendMsg<object> 
    {
        [JsonProperty(PropertyName = "name")]
        public string name;

        [JsonProperty(PropertyName = "key")]
        public string key;

        [JsonProperty(PropertyName = "color")]
        public string color;

        public Join(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
}