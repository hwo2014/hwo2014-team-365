namespace HelloOpenWorld.Models.Output
{
    class Ping: SendMsg<object> {
        protected override string MsgType() {
            return "ping";
        }
    }
}