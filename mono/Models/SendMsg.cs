using Newtonsoft.Json;

namespace HelloOpenWorld.Models
{
    public abstract class SendMsg<T>
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MsgWrapper<object>(this.MsgType(), this.MsgData()));
        }
        protected virtual object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();
    }
}