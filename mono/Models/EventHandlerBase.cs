﻿using HelloOpenWorld.Interfaces;
using HelloOpenWorld.Models.Input;

using Newtonsoft.Json;

namespace HelloOpenWorld.Models
{
    public abstract class EventHandlerBase<T> : IEventHandler
    {
        public SendMsg<object> Handle(string line)
        {
            var result = JsonConvert.DeserializeObject<MsgWrapper<T>>(line);
            return Handle(result.data);
        }

        public abstract SendMsg<object> Handle(T input);
    }
}