﻿using HelloOpenWorld.Models.Input.CarPositions;
using HelloOpenWorld.Models.Input.GameInit;

namespace HelloOpenWorld.GameState
{
    public static class StateHandler
    {
        private static CarPosition[] carPosition;

        public static Race Race { get; set; }

        public static CarPosition[] CarPosition
        {
            get
            {
                return carPosition;
            }
            set
            {
                PreviousPosition = carPosition;
                carPosition = value;
            }
        }

        public static CarPosition[] PreviousPosition { get; private set; }
    }
}
