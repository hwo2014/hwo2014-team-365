﻿using System.Linq;

using HelloOpenWorld.Models.Input.GameInit;

namespace HelloOpenWorld.GameState.PositionControllers
{
    public static class Position
    {
        public static Piece CurrentPiece
        {
            get
            {
                return StateHandler.Race.Track.Pieces[StateHandler.CarPosition[0].PiecePosition.PieceIndex];
            }
        }

        public static Piece NextPiece
        {
            get
            {
                var piecesCount = StateHandler.Race.Track.Pieces.Count();
                var nextIndex = StateHandler.CarPosition[0].PiecePosition.PieceIndex + 1;

                if (piecesCount == nextIndex)
                {
                    nextIndex = 0;
                }

                return StateHandler.Race.Track.Pieces[nextIndex];
            }
        }
    }
}
