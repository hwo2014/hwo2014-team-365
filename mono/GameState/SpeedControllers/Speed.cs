﻿namespace HelloOpenWorld.GameState.SpeedControllers
{
    public static class Speed
    {
        public static double CurrentSpeed
        {
            get
            {
                var previousPosition = StateHandler.PreviousPosition[0].PiecePosition.InPieceDistance;
                var currentPosition = StateHandler.CarPosition[0].PiecePosition.InPieceDistance;

                if (previousPosition > currentPosition)
                {
                    previousPosition -= 100;
                }

                return currentPosition - previousPosition;
            }
        }
    }
}
