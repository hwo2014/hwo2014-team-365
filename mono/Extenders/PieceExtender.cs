﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelloOpenWorld.Models.Input.GameInit;

namespace HelloOpenWorld.Extenders
{
    public static class PieceExtender
    {
        public static bool IsStraight(this Piece piece)
        {
            return piece.Angle == null || piece.Angle == 0;
        }

        public static bool IsCorner(this Piece piece)
        {
            return piece.Angle != null && piece.Angle != 0;
        }

    }
}
