﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

using HelloOpenWorld.Models;

namespace HelloOpenWorld
{
    public static class IOController
    {
        public static List<SendMsg<object>> SendHistory;
        public static List<string> RecievedHistory;

        static IOController()
        {
            SendHistory = new List<SendMsg<object>>();
            RecievedHistory = new List<string>();
        }

        public static StreamReader Reader;

        public static StreamWriter Writer;

        public static void Send(SendMsg<object> msg)
        {
            SendHistory.Add(msg);

            var message = msg.ToJson();
            Writer.WriteLine(message);
        }

        public static void Recieved(string line)
        {
            RecievedHistory.Add(line);
        }
    }
}
