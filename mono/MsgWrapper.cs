using System;

namespace HelloOpenWorld
{
    public class MsgWrapper<T> {
        public string msgType;
        public T data;

        public MsgWrapper(string msgType, T data) {
            this.msgType = msgType;
            this.data = data;
        }
    }
}