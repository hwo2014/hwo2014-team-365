﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelloOpenWorld.GameState;
using HelloOpenWorld.Models;
using HelloOpenWorld.Models.Input;
using HelloOpenWorld.Models.Input.GameInit;
using HelloOpenWorld.Models.Output;

using Newtonsoft.Json;

namespace HelloOpenWorld.EventHandlers
{
    public class GameInitEventHandler : EventHandlerBase<GameInit>
    {
        public override SendMsg<object> Handle(GameInit line)
        {
            StateHandler.Race = line.Race;
            return new Ping();
        }
    }
}
