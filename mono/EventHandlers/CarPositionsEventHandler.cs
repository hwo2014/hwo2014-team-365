﻿using System;

using HelloOpenWorld.Extenders;
using HelloOpenWorld.GameState;
using HelloOpenWorld.GameState.PositionControllers;
using HelloOpenWorld.GameState.SpeedControllers;
using HelloOpenWorld.Models;
using HelloOpenWorld.Models.Input.CarPositions;
using HelloOpenWorld.Models.Output;

namespace HelloOpenWorld.EventHandlers
{
    public class CarPositionsEventHandler : EventHandlerBase<CarPosition[]>
    {
        public override SendMsg<object> Handle(CarPosition[] carPositions)
        {
            StateHandler.CarPosition = carPositions;

            if (Position.NextPiece.IsStraight())
            {
                return new Throttle(1);
            }

            if (Position.CurrentPiece.IsCorner())
            {
                if (((Position.CurrentPiece.Angle > 0) && (Position.NextPiece.Angle < 0)) || ((Position.CurrentPiece.Angle < 0) && (Position.NextPiece.Angle > 0)))
                {
                    var desiredSpeed = (10 - Speed.CurrentSpeed) / (10 - (Math.Abs(Position.NextPiece.Angle.Value) / 10));
                    return new Throttle(desiredSpeed);
                }
            }

            var desiredSpeed2 = (10 - Speed.CurrentSpeed) / (10 - (Math.Abs(Position.NextPiece.Angle.Value) / 11.48));
            return new Throttle(desiredSpeed2);

        }
    }
}
