﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelloOpenWorld.Models;
using HelloOpenWorld.Models.Output;

namespace HelloOpenWorld.EventHandlers
{
    public class FinishEventHandler : EventHandlerBase<object>
    {
        public override SendMsg<object> Handle(object line)
        {
            return new Ping();
        }
    }
}
