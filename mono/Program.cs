﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;

using HelloOpenWorld.Models.Output;

namespace HelloOpenWorld
{
    class Program
    {
        public static void Main(string[] args)
        {
            string host;
            int port;
            string botName;
            string botKey;

            if (args == null || !args.Any())
            {
                host = "testserver.helloworldopen.com";
                port = 8091;
                botName = "SharpShooter";
                botKey = "7cy8zgYQgU+gNQ";
            }
            else
            {
                host = args[0];
                port = int.Parse(args[1]);
                botName = args[2];
                botKey = args[3];
            }

            Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

            using (var client = new TcpClient(host, port))
            {
                var stream = client.GetStream();
                IOController.Reader = new StreamReader(stream);
                IOController.Writer = new StreamWriter(stream);

                IOController.Writer.AutoFlush = true;

                IOController.Send(new Join(botName, botKey));
                var bot = new Bot();
            }

        }
    }
}
