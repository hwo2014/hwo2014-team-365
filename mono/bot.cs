using System;
using System.IO;

using HelloOpenWorld.EventHandlers;
using HelloOpenWorld.Interfaces;
using HelloOpenWorld.Models;
using HelloOpenWorld.Models.Output;

using Newtonsoft.Json;

namespace HelloOpenWorld
{
    public class Bot
    {

        public Bot()
        {
            string line;

            while ((line = IOController.Reader.ReadLine()) != null)
            {
                var msg = JsonConvert.DeserializeObject<MsgWrapper<object>>(line);
                IEventHandler eventHandler;
                switch (msg.msgType)
                {
                    case "carPositions":
                        eventHandler = new CarPositionsEventHandler();
                        break;
                    case "join":
                        eventHandler = new JoinEventHandler();
                        break;
                    case "gameInit":
                        eventHandler = new GameInitEventHandler();
                        break;
                    case "gameEnd":
                        eventHandler = new GameEndEventHandler();
                        break;
                    case "gameStart":
                        eventHandler = new GameStartEventHandler();
                        break;
                    case "yourCar":
                        eventHandler = new YourCarEventHandler();
                        break;
                    case "lapFinished":
                        eventHandler = new LapFinishedEventHandler();
                        break;
                    case "error":
                        eventHandler = new ErrorHandlerBase();
                        break;
                    case "finish":
                        eventHandler = new FinishEventHandler();
                        break;
                    case "tournamentEnd":
                        eventHandler = new TournamentEndEventHandler();
                        break;
                    case "crash":
                        eventHandler = new CrashEventHandler();
                        break;
                    case "spawn":
                        eventHandler = new CrashEventHandler();
                        break;
                    default:
                        eventHandler = null;
                        break;
                }

                if (eventHandler != null) // Console.WriteLine(msg.msgType);
                {
                    IOController.Recieved(line);
                    var result = eventHandler.Handle(line);
                    IOController.Send(result);
                }
            }
        }
    }
}