﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using HelloOpenWorld.Models;

namespace HelloOpenWorld.Interfaces
{
    interface IEventHandler
    {
        SendMsg<object> Handle(string line);
    }
}
